let currentSlide = 0;

function changeSlide(direction) {
    const slides = document.querySelectorAll('.carousel-images img');
    slides[currentSlide].classList.remove('active');

    currentSlide = (currentSlide + direction + slides.length) % slides.length;

    slides[currentSlide].classList.add('active');
}

// Initialisation pour afficher la première image
document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('.carousel-images img')[0].classList.add('active');
});